const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

//routes

//create/add Product
router.post('/',verify,verifyAdmin,productControllers.addProduct);

//get all Products
router.get('/',productControllers.getAllProducts);

//get single Product
router.get('/getSingleProduct/:id',productControllers.getSingleProduct);

//update Product
router.put('/:id',verify,verifyAdmin,productControllers.updateProduct);

//archive
router.put('/archive/:id',verify,verifyAdmin,productControllers.archive);

//activate
router.put('/activate/:id',verify,verifyAdmin,productControllers.activate);

//get active Products
router.get('/active',productControllers.getActiveProducts);

//get inactive Products
router.get('/inactive',verify,verifyAdmin,productControllers.getInactiveProducts)


module.exports = router;