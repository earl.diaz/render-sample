//import User Model
const User = require("../models/User");

//import bcrypt
const bcrypt = require("bcrypt");

//auth module
const auth = require("../auth");


module.exports.registerUser = (req,res) => {


	console.log(req.body);

	if(req.body.password.length < 8){
		return res.send({message:"PW is short"})
	}
	const hashedPW = bcrypt.hashSync(req.body.password,10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW

	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));


}

module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message:"No User Found."})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			console.log(isPasswordCorrect);

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send({message:"Incorrect Password."})
			}
		}

	})
	.catch(err => res.send(err));

}

//allow us to get the details of our logged in user
module.exports.getUserDetails = (req,res) => {

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));


}

module.exports.checkEmailExists = (req,res) => {


	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send("Email is available.");
		} else {
			return res.send("Email is already registered.")
		}

	})
	.catch(err => res.send(err));
}

module.exports.updateUserDetails = (req,res) => {

	console.log(req.body)
	console.log(req.user.id)

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}

module.exports.updateAdmin = (req,res) => {

	console.log(req.params.id)


	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}
